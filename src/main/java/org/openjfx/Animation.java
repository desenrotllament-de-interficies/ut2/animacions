package org.openjfx;

import javafx.animation.FadeTransition;
import javafx.animation.TranslateTransition;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TitledPane;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import javafx.util.Duration;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ThreadLocalRandom;

public class Animation implements Initializable {
    @FXML
    private AnchorPane limite;
    @FXML
    private TitledPane vida;

    @FXML
    private Circle principal;

    @FXML
    private Circle rojo3;

    @FXML
    private Circle rojo2;

    @FXML
    private Circle rojo1;
    private int vidas = 2;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        vida.setText("Te quedan " + (vidas + 1) + " vidas");

        configurarBotones();
        //para cambiar los circulos rojos cada 5seg
        Timer temporizador = new Timer();
        TimerTask tarea = new TimerTask() {
            @Override
            public void run() {
                //insercion de los circulos rojos aleatoriamente
                insertarRojos(rojo1);
                insertarRojos(rojo2);
                insertarRojos(rojo3);
            }
        };

        Integer segundos = 10;

        //temporizadoir para cambiar posicion de los circulos rojos
        temporizador.schedule(tarea, 0, 1000 * segundos);

    }

    public void configurarBotones() {

        this.limite.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent key) {

                TranslateTransition tt = new TranslateTransition(Duration.millis(100));

                //shitch que detecta la pulsacion de cada flecha pulsada
                switch (key.getCode()) {
                    case UP:
//                        System.out.println("ROJO1 X: " + Math.round(rojo1.getBoundsInParent().getMinX() + 5));
//                        System.out.println("PRINCIPAL X: " + Math.round(principal.getBoundsInParent().getMinX()));
//                        System.out.println("--------------------------------------------------------------");
//                        System.out.println("ROJO1 Y: " + Math.round(rojo1.getBoundsInParent().getMinY() + 5));
//                        System.out.println("PRINCIPAL Y: " + Math.round(principal.getBoundsInParent().getMinY()));
                        if (principal.getBoundsInParent().getMinY() > 9) {
                            //Comprueba si el principal y los rojos estan en la misma posicion y suma vidas y los recoloca en otra posicion
                            if (Math.round(principal.getBoundsInParent().getMinY() + 10) == (Math.round(rojo1.getBoundsInParent().getMinY())) + 5) {

                                principal.setRadius(principal.getRadius() + 5);
                                vidas++;
                                insertarRojos(rojo1);
                            }
                            if (Math.round(principal.getBoundsInParent().getMinY() + 10) == (Math.round(rojo2.getBoundsInParent().getMinY())) + 5) {
                                principal.setRadius(principal.getRadius() + 5);
                                vidas++;
                                insertarRojos(rojo2);
                            }
                            if (Math.round(principal.getBoundsInParent().getMinY() + 10) == (Math.round(rojo3.getBoundsInParent().getMinY())) + 5) {
                                principal.setRadius(principal.getRadius() + 5);
                                vidas++;
                                insertarRojos(rojo3);

                            }
                            tt.setToY(principal.getTranslateY() - 10);
                            tt.setNode(principal);
                            tt.playFromStart();
                        } else {
                            if (vidas > 0) {
                                principal.setTranslateX(principal.getParent().getTranslateX());
                                principal.setTranslateY(principal.getParent().getTranslateY());
                                principal.setRadius(principal.getRadius() - 5);
                                vidas--;
                            } else {
                                desvanecer();
                                vidas--;
                            }
                        }

                        break;
                    case DOWN:

                        if (principal.getBoundsInParent().getMinY() < 360) {
                            if (Math.round(principal.getBoundsInParent().getMinY() + 10) == (Math.round(rojo1.getBoundsInParent().getMinY())) + 5) {

                                principal.setRadius(principal.getRadius() + 5);
                                vidas++;
                                insertarRojos(rojo1);
                            }
                            if (Math.round(principal.getBoundsInParent().getMinY() + 10) == (Math.round(rojo2.getBoundsInParent().getMinY())) + 5) {
                                principal.setRadius(principal.getRadius() + 5);
                                vidas++;
                                insertarRojos(rojo2);
                            }
                            if (Math.round(principal.getBoundsInParent().getMinY() + 10) == (Math.round(rojo3.getBoundsInParent().getMinY())) + 5) {
                                principal.setRadius(principal.getRadius() + 5);
                                vidas++;
                                insertarRojos(rojo3);

                            }
                            tt.setToY(principal.getTranslateY() + 10);

                            tt.setNode(principal);
                            tt.playFromStart();
                        } else {
                            if (vidas > 0) {
                                principal.setTranslateX(principal.getParent().getTranslateX());
                                principal.setTranslateY(principal.getParent().getTranslateY());
                                principal.setRadius(principal.getRadius() - 5);

                                vidas--;
                            } else {
                                desvanecer();
                                vidas--;
                            }
                        }


                        break;
                    case LEFT:

                        if (principal.getBoundsInParent().getMinX() > 8) {
                            if (Math.round(principal.getBoundsInParent().getMinY() + 10) == (Math.round(rojo1.getBoundsInParent().getMinY())) + 5) {

                                principal.setRadius(principal.getRadius() + 5);
                                vidas++;
                                insertarRojos(rojo1);
                            }
                            if (Math.round(principal.getBoundsInParent().getMinY() + 10) == (Math.round(rojo2.getBoundsInParent().getMinY())) + 5) {
                                principal.setRadius(principal.getRadius() + 5);
                                vidas++;
                                insertarRojos(rojo2);
                            }
                            if (Math.round(principal.getBoundsInParent().getMinY() + 10) == (Math.round(rojo3.getBoundsInParent().getMinY())) + 5) {
                                principal.setRadius(principal.getRadius() + 5);
                                vidas++;
                                insertarRojos(rojo3);

                            }
                            tt.setToX(principal.getTranslateX() - 10);
                            tt.setNode(principal);
                            tt.playFromStart();
                        } else {
                            if (vidas != 0) {
                                principal.setTranslateX(principal.getParent().getTranslateX());
                                principal.setTranslateY(principal.getParent().getTranslateY());
                                principal.setRadius(principal.getRadius() - 5);
                                vidas--;
                            } else {
                                desvanecer();
                                vidas--;
                            }
                        }
                        break;
                    case RIGHT:

                        if (principal.getBoundsInParent().getMinX() < 350) {
                            if (Math.round(principal.getBoundsInParent().getMinY() + 10) == (Math.round(rojo1.getBoundsInParent().getMinY())) + 5) {

                                principal.setRadius(principal.getRadius() + 5);
                                vidas++;
                                insertarRojos(rojo1);
                            }
                            if (Math.round(principal.getBoundsInParent().getMinY() + 10) == (Math.round(rojo2.getBoundsInParent().getMinY())) + 5) {
                                principal.setRadius(principal.getRadius() + 5);
                                vidas++;
                                insertarRojos(rojo2);
                            }
                            if (Math.round(principal.getBoundsInParent().getMinY() + 10) == (Math.round(rojo3.getBoundsInParent().getMinY())) + 5) {
                                principal.setRadius(principal.getRadius() + 5);
                                vidas++;
                                insertarRojos(rojo3);

                            }
                            tt.setToX(principal.getTranslateX() + 10);
                            tt.setNode(principal);
                            tt.playFromStart();
                        } else {
                            if (vidas > 0) {
                                principal.setTranslateX(principal.getParent().getTranslateX());
                                principal.setTranslateY(principal.getParent().getTranslateY());
                                principal.setRadius(principal.getRadius() - 5);

                                vidas--;
                            } else {

                                desvanecer();
                                vidas--;

                            }
                        }
                        break;
                }


                //mostramos el fin del juego
                if (vidas==-1) {
                    vida.setText("No Te quedan vidas!!");
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setTitle("SE ACABO!");
                    alert.setContentText("GAME OVER");
                    alert.showAndWait();
                }else{
                    vida.setText("Te quedan " + (vidas + 1) + " vidas");
                }


            }

        });


    }

    //metodo que nos inserta en una posicion aleatoria
    public void insertarRojos(Circle rojito) {
        double random = ThreadLocalRandom.current().nextDouble(400);
        double x = random + principal.getLayoutX();
        double y = random + principal.getLayoutY();

        if (x > 400 || y > 400) {
            x = random;
            y = random;
        }
        if (x == 0 || y == 0) {
            x = ThreadLocalRandom.current().nextDouble(400);
            y = ThreadLocalRandom.current().nextDouble(400);
        }
        rojito.setLayoutX(x);
        rojito.setLayoutY(y);


    }

    //funcion para desvanecer el circulo principal al acabar
    public void desvanecer() {
        FadeTransition fot = new FadeTransition();
        fot.setDuration(Duration.millis(2000));
        fot.setFromValue(1);
        fot.setToValue(0.1);

        fot.setNode(principal);
        fot.play();
        principal.setDisable(true);
    }


}
